#compdef zig

#function _zig () {

#  _arguments -C \
#    '-h[Print command-specific usage]' \
#    '--help[Print command-specific usage]'
   #'init-exe[Initialize a `zig build` application in the cwd]' \
   #'init-lib[Initialize a `zig build` library in the cwd]' \
   #'ast-check[Look for simple compile errors in any set of files]' \
   #'build-exe[Create executable from source or object files]' \
   #'build-lib[Create library from source or object files]' \
   #'build-obj[Create object from source or object files]' \
   #'fmt Reformat Zig source into canonical form]' \
   #'run Create executable and run immediately]' \
   #'test Create and run a test build]' \
   #'translate-c Convert C code to Zig code]' \
   #'ar[Use Zig as a drop-in archiver]' \
   #'cc[Use Zig as a drop-in C compiler]' \
   #'c++[Use Zig as a drop-in C++ compiler]' \
   #'dlltool[Use Zig as a drop-in dlltool.exe]' \
   #'lib[Use Zig as a drop-in lib.exe]' \
   #'ranlib[Use Zig as a drop-in ranlib]' \
   #'env[Print lib path, std path, cache directory, and version]' \
   #'help[Print this help and exit]' \
   #'libc[Display native libc paths file or validate one]' \
   #'targets[List available compilation targets]' \
   #'version[Print version number and exit]' \
   #'zen[Print Zen of Zig and exit]'
              #'build[Build project from build.zig]' \

#}

function _zig () {
  local line state

  _arguments -C \
    '-h[Print command-specific usage]' \
    '--help[Print command-specific usage]' \
    "1: :->cmds" \
    "*::arg:->args"

  case "$state" in
     cmds)
       _values 'command' \
               "init-exe[Initialize a \`zig build\` application in the cwd]" \
               "init-lib[Initialize a \`zig build\` library in the cwd]" \
               'ast-check[Look for simple compile errors in any set of files]' \
               'build-exe[Create executable from source or object files]' \
               'build-lib[Create library from source or object files]' \
               'build-obj[Create object from source or object files]' \
               'fmt[Reformat Zig source into canonical form]' \
               'run[Create executable and run immediately]' \
               'test[Create and run a test build]' \
               'translate-c[Convert C code to Zig code]' \
               'ar[Use Zig as a drop-in archiver]' \
               'cc[Use Zig as a drop-in C compiler]' \
               'c++[Use Zig as a drop-in C++ compiler]' \
               'dlltool[Use Zig as a drop-in dlltool.exe]' \
               'lib[Use Zig as a drop-in lib.exe]' \
               'ranlib[Use Zig as a drop-in ranlib]' \
               'env[Print lib path, std path, cache directory, and version]' \
               'help[Print this help and exit]' \
               'libc[Display native libc paths file or validate one]' \
               'targets[List available compilation targets]' \
               'version[Print version number and exit]' \
               'zen[Print Zen of Zig and exit]'
        ;;
      args)
        case ${line[1]} in
          ast-check)
            _astcheck_cmd
            ;;
          init-exe)
            _initexe_cmd
            ;;
          init-lib)
            _initlib_cmd
            ;;
          fmt)
            _fmt_cmd
            ;;
        esac
        ;;
  esac
}


function _fmt_cmd() {
  _arguments  {-h,--help}'[Print help for this command]' \
              "(--color)--color[\[auto|off|on\] Enable or disable colored error messages]" \
              '(--stdin)--stdin[Format code from stdin; output to stdout]' \
              '(--check)--check[List non-conforming files and exit with an error]' \
              '(--ast-check)--ast-check[Run zig ast-check on every file]' \
              '(--exclude)--exclude[Exclude file or directory from formatting]'
}


function _initexe_cmd() {
    _arguments {-h,--help}'[Print help for this command]'
}

function _initlib_cmd() {
    _arguments {-h,--help}'[Print help for this command]'
}

function _astcheck_cmd() {
    _arguments {-h,--help}'[Print help for this command]' \
               "(--color)--color[\[auto|off|on\] Enable or disable colored error messages]" \
               '(-t)-t[(debug option) Output ZIR in text form to stdout]'
}
